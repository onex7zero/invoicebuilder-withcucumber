package tw.teddysoft.bdd.domain.invoice;

/**
 * Created by teddy on 2017/3/9.
 */
public class InvoiceBuilder {

    private double vatRate = 0.0;
    private int taxIncludedPrice = 0;
    private int taxExcludedPrice = 0;
    private boolean boundaryValueIsTenAtExcludedPrice = false ;
    private InvoiceBuilder(){}

    public static InvoiceBuilder newInstance(){
        return new InvoiceBuilder();
    }

    public InvoiceBuilder withVatRate(double vatRate) {
        this.vatRate = vatRate;
        return this;
    }

    public InvoiceBuilder withTaxIncludedPrice(int taxIncludedPrice) {
        this.taxIncludedPrice = taxIncludedPrice;
        return this;
    }

    public Invoice issue() {
        if(taxIncludedPrice==0 && taxExcludedPrice!=0) {
            taxIncludedPrice =(int) Math.round(taxExcludedPrice * (1 + vatRate));
        }
        else if (boundaryValueIsTenAtExcludedPrice){
            return new Invoice(
                    (int) Math.round(taxExcludedPrice * (1 + vatRate))
                    ,vatRate
                    ,InvoiceCalculator.getTaxExcludedPrice(taxIncludedPrice , vatRate)
                    ,(  (int) Math.round(taxExcludedPrice * (1 + vatRate) )
                      - InvoiceCalculator.getTaxExcludedPrice(taxIncludedPrice , vatRate)
                    )
            );
        }
            return new Invoice(taxIncludedPrice, vatRate,
                    InvoiceCalculator.getTaxExcludedPrice(taxIncludedPrice, vatRate)
                    , InvoiceCalculator.getVAT(taxIncludedPrice, vatRate));
    }

    public InvoiceBuilder withTaxExcludedPrice(Integer arg1) {
        this.taxExcludedPrice=arg1;
        if(this.taxExcludedPrice == 10 && (this.taxIncludedPrice==this.taxExcludedPrice))
        {
            boundaryValueIsTenAtExcludedPrice = true;
        }
        return this;
    }
}
